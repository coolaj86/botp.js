# You might be in the wrong place

You probably want [Authenticator](https://github.com/Daplie/browser-authenticator).

# Browser One Time Password library (JavaScript)

(aka botp / totp.js / hotp.js)

(forked from [Node One Time Password](https://github.com/guyht/notp))

Simple to use, fast, and with zero dependencies\*. The Browser One Time Password library is fully compliant with [HOTP](http://tools.ietf.org/html/rfc4226) (counter based one time passwords) and [TOTP](http://tools.ietf.org/html/rfc6238) (time based one time passwords).

\* requires [forge](https://github.com/digitalbazaar/forge) for window.sha1Hmac shim in older browsers and [es6-promise](https://github.com/jakearchibald/es6-promise) for ancient browsers.

It can be used in conjunction with the [Authy](https://www.authy.com/personal/), [Google Authenticator](https://github.com/google/google-authenticator/), and [Microsoft Authenticator](https://www.microsoft.com/en-us/store/apps/authenticator/9wzdncrfj3rj), and [GAuth](https://5apps.com/gbraad/gauth) which have free apps for iOS, Android, BlackBerry, OS X, Linux, Windows, and Chrome.

Browser One Time Password library, supports HOTP, TOTP and works with Google Authenticator • forked from https://github.com/guyht/notp

# Installation

```
bower install botp
```

# Usage

```javascript
(function () {
'use strict';

var botp = window.botp;

// this might be used on account creation to create the QR code and verify the token in the browser.

var key = 'secret key for user... could be stored in DB'; // Uint8Array
var token = 'user supplied one time use token'; // 890123

// Check TOTP is correct (HOTP if hotp pass type)
botp.totp.verify(token, key).then(function (login) {
  // invalid token if login is null
  if (!login) {
    console.log('Token invalid');
    return;
  }
  
  // valid token
  console.log('Token valid, sync value is %s', login.delta);
});

}());
```

# API

See <https://github.com/guyht/notp#api>

* botp.totp.gen(keyByteArray) => (promise) tokenArray
* botp.totp.verify(tokenByteArray, keyByteArray) => (promise) delta or null
* botp.hotp.gen(keyByteArray) => (promise) tokenArray
* botp.hotp.verify(tokenByteArray, keyByteArray) => (promise) delta or null
